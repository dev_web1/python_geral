"""
        LEN - RETORNA O NR DE CARACTERE DE UMA STRING
        Strip - retira espaços vazios no inicio e fim
"""
var1 = 'Rio Grande'
print(f'{var1} tem {len(var1)} caracteres')
"""
        var.isdigit() - verifica se existem somente numeros em var
"""
num1 = input('Digite um número. ')
num2 = input('Digite outro número ')
"""
if num1.isdigit() and num2.isdigit():
    print(f'Soma {num1} e {num2} é: {int(num1) +int(num2)}')
else:
    print(('Não foi possivel converter os tipos de dados.'))
"""
try:
    num1 = float(num1)
    num2 = float(num2)
    print(num2 + num1)
except:
    print('ERROR!!!!')

"""
    pass ou ... para escrever o código posteriormente
"""
print('¨¨¨¨¨¨¨¨¨¨¨¨¨')

if False:
    pass
else:
    print('Tchau"!')

print('¨¨¨¨¨¨¨¨¨¨¨¨¨')