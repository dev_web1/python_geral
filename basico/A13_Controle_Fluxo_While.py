"""
    FLUXO DE DADOS OU LAÇOS DE REPETIÇÃO:
    WHILE (ENQUANTO) FOR (PARA)
"""
x = 0
while x < 10:
    print(f'O valor de x = {x}')
    x = x + 1
print('11111111111111111111111111')

x=0
# usamor 'continue' para pular uma volta no laço
while x < 10:
    if x == 3:
        x = x + 1
        continue
    print(f'O valor de x = {x}')
    x = x + 1


print(('imprimindo pares ordenados').upper())

"""
 NESSE LOOP PARA CADA VOLTA PARA O X O Y ANDA TRES VOLTAS OU SEJA IMPRIMIRA
 (0,0)(0,1)(0,2) (1,0) (1,1) (1,2)
"""
x=0#  coluna
while x < 2:
    y = 0  #linha DEPOIS DE FINALIZAR O LOOP DO Y REINCIA O X E O Y RECEBE ZERO PARA INIIAR DE NOVO.
    while y < 3:
        print(f'({x},{y})')
        y = y + 1

    x = x + 1
print('Terminou!')
print()
"""
    WHILE / ELSE
"""
print('WHILE / ELSE')
contador=0
while contador < 10:
    print(f'contador = {contador}')
    contador += 1
else:
    print('Chegou no else!')