"""
    Algorítimo Verificador do Cpf -
    CPF
--------------------------------
PARTE I
--------------------------------
selecionar os primeiros nove digitos do cpf sem ponto, virgula ou traço
exemplo: 168995350
para descobrir o digito verificador 10°
faz o cálculo abaixo

1 * 10 = 10
6 *  9 = 54
8 *  8 = 64
9 *  7 = 63
9 *  6 = 54
5 *  5 = 25
3 *  4 = 12
5 *  3 = 15
0 *  2 =  0
 soma   ----
        297

11 - (297 % 11) = 11

se  o resultado ->11 > 9 então o 10° digito é 0 logo fica 1689953500

---------------------------------
PARTE II
--------------------------------
para descobrir o digito verificador 11°
faz o cálculo abaixo:
1 * 11 = 11
6 * 10 = 60
8 *  9 = 72
9 *  8 = 72
9 *  7 = 63
5 *  6 = 30
3 *  5 = 15
5 *  4 = 20
0 *  3 =  0
0 *  2 =  0
 soma   ----
        343

11 - (343 % 11) =  9 (resultado)
if o resultado > 9  a resposta é 0
if o resultado <= 9 a resposta é o resultado
logo como resultado é igual a nove o 11° digito do cpf é o próprio 9 ficanoo
 16899535009
"""
cpf = input('Digite os digitos do cpf sem . ou - : ')

testa_cpf = ''
letra = ''

for letra in cpf:
    if (letra != '.') and (letra != '-'):
        testa_cpf = testa_cpf + letra
print(testa_cpf)

if len(testa_cpf) != 11:
    print('O cpf deve ter 11 digitos.')
    exit()
    #  nove primeiros digitos
nove_digitos = testa_cpf[0:9]
print(nove_digitos)

# PARTE I - VERIFICANDO O 10 DIGITO DO CPF
letra = ''
soma_digitos = 0
contador = 10
for letra in nove_digitos:
    soma_digitos = soma_digitos + (int(letra) * contador)
    contador = contador - 1

#  se resultado é menor ou igual a nove o 10° digito do cpf é o próprio resultado, se
#  for maior  fica zero
resultado_digito10 = 11 - (soma_digitos % 11)
if resultado_digito10 <= 9:
    dez_digitos = nove_digitos + str(resultado_digito10)
else:
    dez_digitos = nove_digitos + str(0)

print(f'soma: {soma_digitos}, resultado: {resultado_digito10}, cpf10:{dez_digitos}')

# PARTE II - VERIFICANDO O 11º DIGITO DO CPF
letra = ''
soma_digitos = 0
contador = 11
for letra in dez_digitos:
    soma_digitos = soma_digitos + (int(letra) * contador)
    contador = contador - 1

#  se resultado é menor ou igual a nove o 10° digito do cpf é o próprio resultado, se
#  for maior  fica zero
resultado_digito11 = 11 - (soma_digitos % 11)
if resultado_digito11 <= 9:
    onze_digitos = dez_digitos + str(resultado_digito11)
else:
    onze_digitos = dez_digitos + str(0)

print(f'soma: {soma_digitos}, resultado: {resultado_digito11}, cpf11:{onze_digitos}')

if (testa_cpf == onze_digitos):
    print('O Cpf é válido!')
else:
    print('Cpf inválido')
