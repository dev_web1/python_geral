"""
OPERADORES ARITMÉTICOS
+ SOMA
- SUBTRAÇÃO
* MULTIPLICAÇÃO
/ DIVISÃO
// DIVISÃO ARREDONDANDO PARA INTEIRO
** POTENCIAÇÃO
%  RESULTADO DA DIVISÃO
() ALTERA A PRECEDÊNCIA DE EXECUÇÃO DO CÁLCULO
"""
valorA= 20
valorB= 5
print(' a + b = ', valorB + valorA )
print(' a - b = ', valorA - valorB )
print(' a * b = ', valorA * valorB )
print('a / b = ', valorA / valorB )
print(' a ** b = ', valorA ** valorB )