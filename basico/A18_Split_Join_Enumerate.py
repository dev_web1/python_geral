"""
    SPLIT, JOIN, ENUMERATE IN PYTHON
"""
texto = '0 Brasil é pentacampeão do Mundo. O Brasil tem um riqueze imensa: o povo diversificado.'
#  Split - separa a palavra e letra que tem espaços entre elas
print(texto.split(' '))
#  Split - separa a palavra e letra que tem '.' entre elas
print(texto.split('.'))

# Join - serve para juntar elemento de uma lista
string = 'O amor é um sentimento sublime, que somente os puros de coração desenvolvem.'
lista = string.split(' ')
#  com o join vamos juntar os elementos da lista
string2 = ' '.join(lista)
print(string)
print(lista)
print(string2)

# Enumerate - enumera elementos de uma lista
print('-----------  ENUMERATE----------')
for indice, valor in enumerate(lista):
    print(indice, valor)

for indice, valor in enumerate(texto):
    print(indice, valor)

print('-------------------------------')
#  Lista dentro de outra lista
lis = [
    [2,3], [4,5], [6,7]
]
#  imprimindo a lista q esta dentro da lista
for v in lis:
    print(v)

print(' ')
print('-------------------------------')
#  imprimindo os elementos da lista q esta dentro da lista
for v in lis:
    print(v[0], v[1])

print(' ')
print('-------------------------------')
#  desempacotando elemento de uma lista - obtendo o mesmo resultado do enumerate que retorna tuple e este
#  retorna lista
lista = [
    [0, 'Claudio'], [1,'Emanuelle'], [2,'Regina Freitas']
]
for indice, valor in lista:
    print(indice, valor)

print(' ')
print('-------------------------------')
lista = [
    ['Claudio'], ['Emanuelle'], ['Regina Freitas']
]
for indice, valor in enumerate(lista):
    print(indice, valor)

print(' ')
print('-------------------------------')
lista = [
    #  0            1              2
    ['Claudio'], ['Emanuelle'], ['Regina Freitas'],              # 0
    ['Carlos'], ['Gilberto'], ['Gismonti'],                      # 1
    ['Fernanda'], ['Emanuelle Nascimento'], ['Angela Freitas'],  # 2
]

lista2 = [
    #  0            1              2
    ['Claudio', 'Emanuelle', 'Regina Freitas'],              # 0
    ['Carlos', 'Gilberto', 'Gismonti'],                      # 1
    ['Fernanda', 'Emanuelle Nascimento', 'Angela Freitas'],  # 2
]

for indice, valor in enumerate(lista):
    print(indice, valor)

print(' ')
print('------------IMPRIMINDO UM DETERMINADO ELEMENTO DA LISTA -------------------')
#  imprimindo um elemento da lista
print(type(lista))
print(lista[8])
print('-------------------------------')

#  usando um type cast convertemos o objet para list
enumerada = list(enumerate(lista))
print(f'Lista normal: {lista}')
print(f'Lista enumerada: {enumerada}')
print(f'Item da lista enumerada: {enumerada[1][1]}')

"""
é impresso na tela várias tuplas:
[(0, ['Claudio']), (1, ['Emanuelle']), (2, ['Regina Freitas']), (3, ['Carlos']), (4, ['Gilberto']), 
(5, ['Gismonti']), (6, ['Fernanda']), (7, ['Emanuelle Nascimento']), (8, ['Angela Freitas'])]

"""
print('-------------------------------')
lista2 = [
    #  0            1              2
    ['Claudio', 'Emanuelle', 'Regina Freitas'],              # 0
    ['Carlos', 'Gilberto', 'Gismonti'],                      # 1
    ['Fernanda', 'Emanuelle Nascimento', 'Angela Freitas'],  # 2
]
#  usando um type cast convertemos o objet para list
enumerada = list(enumerate(lista2))
print(f'Lista normal: {lista2}')
print(f'Lista enumerada: {enumerada}')
print(f'Item da lista enumerada[1][1]: {enumerada[1][1]}')
print(f'Elemento da sub - lista enumerada[1][1][2]: {enumerada[1][1][2]}')
print(f'Indice da lista enumerada[0][0]: {enumerada[0][0]}')
print('-------------------------------')

for v1, v2 in enumerate(lista2):
    print(v1, v2)

print('-------------------------------')
#  posso indicar outra enumeração no caso a partir do 25
for v1, v2 in enumerate(lista2, 25):
    print(v1, v2)

print('--------outra forma de obter o mesmo resultado acima -----------------------')
for v1 in enumerate(lista2, 25):
    valor_enumerado, minha_lista = v1
    print(valor_enumerado, minha_lista)

print('-------- imprimindo a(s) colunas da sub-lista -----------------------')
for v1 in enumerate(lista2, 25):
    valor_enumerado, minha_lista = v1
    coluna1, coluna2, coluna3 = minha_lista
    print(coluna2)
    print(coluna3)


