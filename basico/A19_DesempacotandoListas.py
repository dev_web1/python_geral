"""
    DESEMPACOTAMENTO DE LISTAS EM PYTHON
"""
lista = ['amor', 'fraternidade', 'amizade']
#  desempacotando a lista em variaves correspondentes a cada elemento da lista
n1, n2, n3 = lista
print(n1)
print('-------------------------')
#  desempacotando a lista em variaves correspondentes a cada elemento da lista
lista = ['amor', 'fraternidade', 'amizade',1,1,2,3,4,5,200]
n1, n2, *sublista = lista
print(n1, n2, sublista)
n1, *sub, ultimo = lista
print(f'SubLista e o ultimo da lista:{sub}, {ultimo}')
*outraLista, n1, n2  = lista
print(f'Ultimos da lista:{n1}, {n2}')