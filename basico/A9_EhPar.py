num = input('Digite um número inteiro. ')
try:
    num = int(num)
    if (num % 2) == 0:
        print(f' o número {num} é par')
    else:
        print(f' o número {num} é impar')
except:
    print('Você não digitou um número inteiro.')
