# isto é um comentário de linha
# print('Boa noite turma!')  # OUTRO COMENTŔIO
# para repetir uma mesma linha: posicionamos o cursor na linha e teclamso Ctrl + d

print("Linha 1")

''' Comentario de bloco com aspas simples
print("Linha 2")
'''

print("Linha 3")

""" BLOCO DE COMENTÁRIO com Aspas Duplas
print("Linha 4")
print("Linha 5")
"""
