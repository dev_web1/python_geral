"""
    AS VARIÁVEIS EM PYTHON PODEM INICIAR LETRAS,
    PODEM CONTER NUMEROS, MAS NÃO INICIAR POR ELES,
     PODEM  SEPARAR COM  _, E LETRA
    MINÚSCULAS
"""
nome = 'CLAUDIO SAMUEL'
NOME = 'PEDRO'
idade = 57
altura = 1.72
eh_maior = idade > 18
peso = 70
imc = 0.00
imc = peso /(altura ** 2)
ano = 2022
nascimento = ano - idade;
print('Nome:', nome, NOME, 'Idade: ', idade, 'Altura: ', altura, 'Eh maior: ',eh_maior, 'Imc sem formatação =', imc)
#  para formatarmos números reais  com duas casas decimais utilzamor :.2f depois da variável
#  uso do F-Strings(com chaves)
print(f'Nome {nome} tem {idade} anos, nasceu em {nascimento } e seu imc é {imc:.2f}')
#  outra forma de fazer é
print('{} tem {} e seu imc é {:.2f} '.format(nome, idade,imc) )
print(type("""48"""))
print(type(NOME))
