"""
    FORMATANDO VALORES COM MODIFICADORES
    :s - String(texto)
    :d - Inteiros (int)
    :f - Números de ponto flutuante (float)
    :.(quantidade de caracteres)f - quantidades de casas decimais (float)
    >  Esquerda
    <  Direita
    ^  Centro
    Exempolos:

"""
# formatando strings
nome = 'claUDIO  SaMUel'
print(f'{nome:s}')

# padroniza em 20 caracteres completando com $ a direita o que falta
print(nome.ljust(20, "$"))

# coloca tudo em minúsculo
print(nome.lower())

# coloca tudo em maiúsculo
print(nome.upper())
# coloca a primeira letra em caixa alta

print(nome.title())
print(nome.capitalize())

num1 = 1
num2 =20
divisao = num1 / num2

# formatanado o resultado com quatro casas decimais
print('{:.4f}'.format(divisao))

# outra forma que apresenta a mesma formatação
num1 = 1
num2 =1227
divisao = num1 / num2
print(f'{divisao:.10f}')

# preenchendo com zeros a esquerda ,direita ou nas pontas
print(f'{num2:0>10}')
print(f'{num2:0<10}')
print(f'{num2:0^10}')

"""
    FATIANDO STRINGS
"""
print('%%%%%%%%%%%%%%%%%%')
texto = 'Rio Grande'
#  imprime da posição 0  ate a pos 2 da string
parte_texto = texto[0:3]
print(parte_texto)
 #  imprime pulado de 3 em 3
print(texto)
print(texto[0::2])