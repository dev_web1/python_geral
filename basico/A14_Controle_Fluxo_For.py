"""
    CONTROLE DE FLUXO  COM FOR EM PYTHON
    ITERANDO STRINGS COM FOR
    FUNÇÃO RANGE(start, stop, steep)
"""
texto = 'amizada'
#  conseguimos o mesmo efeito qdo utilzamos o while para percorrer a
#  string pelo indice
for n, letra in enumerate(texto):
    print(n, letra)

print('------------------------')
for n in texto:
    print(n)

print('---  Sequència de 0 a 100 pulando de 3 em 3   --------')
for n in range(0,100,3):
    print(n)

print('---------   Número entre 0 e 100 divisíveis por 8  ---------------')
for n in range(100):
    if n % 8 == 0:
        print(n)
print('------------------------')

novo_texto = 'python'
letra = ''
nova_string = ''
for letra in novo_texto:
    if letra == 'p':
        nova_string = nova_string + letra.upper()
    elif letra == 't':
        nova_string = nova_string + letra.upper()
    else:
        nova_string += letra
print(nova_string)

