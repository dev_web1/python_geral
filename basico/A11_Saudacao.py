hora = input('Informe a hora (0-23)? ' )

try:
    hora = int(hora)
    dia = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
    tarde= [13, 14, 15, 16, 17]
    noite= [18, 19, 20, 21, 22, 23]
    if hora in dia:
        print('Bom dia!')
    elif hora in tarde:
        print('Bom tarde!')
    elif hora in noite:
        print('Bom noite!')
    else:
        print('Não reconheço essa hora no meu sistema.!')
except:
    print('A hora não é válida.')