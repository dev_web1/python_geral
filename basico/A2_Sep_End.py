'''
    sep e end são argumentos da função print que representam respectivamente "separador" e "fim".
'''
print(12345)
print('Maria', "JOsé",'Pedro', sep='-')
print('Maria', "JOsé",'Pedro', sep='-', end='*****\n')
print('464','011','300', sep='.', end='-')
print('59')

print("Esta é uma 'string' ok.")

print('Esta é outra "string" ok.')

