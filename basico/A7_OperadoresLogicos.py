"""
    OPERADORES LÓGICOS
    AND, OR, NOT, IN,E NOT IN
"""
exp1 = True
exp2 = False
print(exp2 and exp1)

print('-------')

print(exp1)
if not exp1:
    print('é falso')
else:
    print('é verdadeiro')
print('-------')

a=5
b=2
if not a>b:
    print('b maior que a')
else:
    print('a maior que b')

c = False
d = True # Zero é considerado um boleano falso.
print('********')
if not c == d:
    print('UM')
else:
    print('dois')
print('-*****-')
# testamdp variáveos vazias
if not d:
    print( d, 'esta vazio')
if not c:
    print('preencho o valor de c')

print()
texto = 'amor'
if 'r' in texto:
    print('existe')

if 'ar' not in texto:
    print('Não existe.')

