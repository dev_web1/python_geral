"""
 TIPOS DE DADOS
 str - String: 'texto', 'casa', 'Rio Grande'
 int - Inteiros: 1, 2 ,4, 50, 100
 float - ponto flutuante: 1.5, 0.5
 boolean - lógico: true/false
"""

print('string', type('string'))
print('int', type(10))
print('float', type(2.50))
print(10==10, type(10==10))
print('l'=='L', type('l'=='L'))

# convertendo numero string em inteiro
print('20', type('20'), type(int('20')))
print('10', float('20'))
print('-------------------')
print('Samuel', type('Samuel'))
print(56, type(56))
print(1.72, type(1.72))
print(56>18, type(56>18))
