"""
    ARGUMENTOS *ARGS E *KWARGS
    SÃO UTILIZADO QUANDO NÃO TEMOS O NRO CERTO DE PARAMETROS QUE NOSSA FUNÇÃO
    IRÁ RECEBER
"""
def func(*args, **kwargs):
    print(args)
    print(kwargs['nome'], kwargs['sobrenome'])
#  verficando se foi enviado determinado parametro
    #  caso não tenha sido enviado é impresso none
    idade = kwargs.get('idade')
    if idade is not None:
        print(idade)
    else:
        print('Parametro idade não existe')

    sobrenome = kwargs.get('sobrenome')
    if sobrenome is not None:
        print(sobrenome)
    else:
        print('Parametro idade não existe')

lista = [1, 2, 3, 4, 5]
lista2 = [10, 20, 30, 40, 50]
func(*lista, *lista2, nome='Claudio',sobrenome = 'Freitas')