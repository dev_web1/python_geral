"""
    OPERADOR TERNÁRIO
"""
#  EXEMPLO
logged_user = True
if logged_user:
    msg = 'Usuário logado.'
else:
    msg = 'Usuário precisa logar.'

print(msg)

#  utilizando o operador ternáiro
msg = 'Usuário logado.' if logged_user else 'Usuário precisa logar.'
print(msg)
#  exemplo-2
idade = 17.999999
msg = 'Pode acessar.' if idade >= 18 else 'Não pode acessar.'
print(msg)

#  exemplo-3
idade = 19
eh_de_maior = (idade >= 18)
msg = 'Pode acessar.' if eh_de_maior else 'Não pode acessar.'
print(msg)

