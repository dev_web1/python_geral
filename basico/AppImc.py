'''
    Valor do IMC	Classificação
    Abaixo de 17	Muito abaixo do peso
    Entre 17 e 18,49	Abaixo do peso
    Entre 18,5 e 24,99	Peso normal
    Entre 25 e 29,99	Acima do peso
    Entre 30 e 34,99	Obesidade I
    Entre 35 e 39,99	Obesidade II (severa)
    Acima de 40         Obesidade III - Mórbida
'''

de_nov='s'
while(de_nov == 's'):
    peso = float(input('Informe seu peso: '))
    altura = float(input('Informe sua altura: '))
    imc = peso / (altura * altura)
    print('Seu IMC é: ',imc)
    if (imc <= 17):
        print('Muito abaixo do peso.')
    elif(imc > 17) and (imc < 18.5):
        print('Abaixo do Peso.')
    elif (imc >= 18.5 and imc < 25):
        print('Peso Normal.')
    elif (imc >= 25 and imc < 30):
        print('Acima do peso.')
    elif( imc >= 30 and imc < 35):
        print('Obesidade I.')
    elif( imc >= 35 and imc < 40):
        print('Obesidade II - Severa')
    else:
        print('Obesidade III - Mórbida')
    de_nov=input('Deseja continura?s/n ')

    if de_nov == 'n':
        print('Agora acabou.')

