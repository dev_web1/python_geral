# importar o App, Builder(GUI)
# criar o aplicativo
# criar a função build

import requests
from kivy.app import App
from kivy.lang import Builder
from pkg_resources import resource_filename

# retorna o caminho absoluto do arquivo
resource_1 = resource_filename(__name__, 'tela.kv')
print(resource_1)
GUI = Builder.load_file(resource_1)


class MeuAplicativo(App):
    def build(self):
        return GUI

    def on_start(self):
        self.pegar_cotacao("USD")
        self.root.ids["moeda1"].text = f"One Dólar     - R$ {self.pegar_cotacao('USD')}"
        self.root.ids["moeda2"].text = f"One Euro      - R5 {self.pegar_cotacao('EUR')}"
        self.root.ids["moeda3"].text = f"One Bitcoin   - R$ {self.pegar_cotacao('BTC')}"
        self.root.ids["moeda4"].text = f"One Ethereum  - R$ {self.pegar_cotacao('ETH')}"

    def pegar_cotacao(self, moeda):
        link = f"https://economia.awesomeapi.com.br/last/{moeda}-BRL"
        requisicao = requests.get(link)
        dic_requisicao = requisicao.json()
        cotacao = dic_requisicao[f"{moeda}BRL"]["bid"]
        return cotacao


MeuAplicativo().run()


