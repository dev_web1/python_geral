import jogoqualnumero
import forca
import time
#### ESCREVENDO NO ARQUIVO
#arquivo=open('palavras.txt', 'w')
#arquivo.write('melancia\n')
#arquivo.write('laranja\n')
#arquivo.write('abacate\n')
#arquivo.write('morango\n')
#arquivo.close()
#### LENDO O ARQUIVO
#arquivo = open('palavras.txt', 'r')
#for linha in arquivo:
#    print(linha)
#arquivo.close()
continuar = True
while  continuar == True:        
    print('*************************************')
    print('**********  MENU DE JOGOS   **********')
    print('*************************************')
    print('****      1. Adivinha o número  ***********')
    print('****      2. Forca                    ***********')
    print('****  Escolha um dos números do jogo  ****')
    print('*************************************')
    
    escolha = int(input('Qual jogo quer jogar? Digite o número : '))

    if escolha == 1:
        jogoqualnumero.adivinha()
    elif escolha == 2:
        forca.jogar()

    jogar = str(input('Quer continuar jogando? s / n: '))

    if jogar.upper() == 'S':
        continuar = True
    else:
        continuar = False
        print('O aplicativo será encerrado...')
        time.sleep(1)
        exit()
        
