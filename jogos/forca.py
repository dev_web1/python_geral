import random
import time
totalletras=0

def imprime_mensagem_abertura():
    print('*********************************')
    print('***Bem vindo ao jogo da Forca!***')
    print('*********************************')

def carrega_palavra_secreta():
    arquivo = open('palavras.txt', 'r')
    palavras = []
    for linha in arquivo:
        linha = linha.strip()
        palavras.append(linha)
    arquivo.close()
    numero = random.randrange(0, len(palavras))
    palavra_secreta = palavras[numero].upper()
    return palavra_secreta

def inicializa_letras_acertadas(palavra):
    return ['_' for letra in palavra]

def pede_chute():
    chute = input('Qual letra? ')
    chute = chute.strip().upper()
    return chute

#def marca_chute_correto(chute, letras_acertadas, palavra_secreta):
#    posicao = 0
#    for letra in palavra_secreta:
#        if (chute == letra):
#            letras_acertadas[posicao] = letra
#            posicao += 1
def marca_chute_correto(chute, letras_acertadas, palavra_secreta):
    posicao = 0
    for letra in palavra_secreta:
        if (chute.upper() == letra.upper()):
            letras_acertadas[posicao] = letra
        posicao = posicao + 1


def imprime_mensagem_perdedor(palavra_secreta):
    print('')
    print('----------------------------------------')
    print('****    Puxa, você foi enforcado!  *****')
    print('****    A palavra era {}'.format(palavra_secreta)+'   ****')
    print('----------------------------------------')

def imprime_mensagem_vencedor():
    print('*** Parabéns, você ganhou! ***')
    print("         ___________         ")
    print("        '._==_==_=_.'        ")
    print("        .-\\     :/-.        ")
    print("      | (|:.        |) |     ")
    print("       '-|:.|-        '      ")
    print("         \\::.      /        ")
    print("          '::.    .'         ")
    print("              ) (            ")
    print("            _.' '._          ")
    print("           '-------'         ")
    time.sleep(2)

def jogar():
    palavra_secreta = carrega_palavra_secreta()
    imprime_mensagem_abertura()
    totalletras=len(palavra_secreta)
    letras_acertadas = inicializa_letras_acertadas(palavra_secreta)
    print('A palavra tem :' + str(totalletras) + ' letras')
    print(letras_acertadas)

    enforcou = False
    acertou = False
    erros = 0

    while not enforcou and not acertou:
        chute = pede_chute()
        if (chute in palavra_secreta):
            marca_chute_correto(chute, letras_acertadas, palavra_secreta)
        else:
            erros += 1
        enforcou = erros == 6
        acertou = '_' not in letras_acertadas
        print(letras_acertadas)

    if(acertou):
        imprime_mensagem_vencedor()
    else:
        imprime_mensagem_perdedor(palavra_secreta)

    print('Fim do jogo')
